import { describe, expect, it } from "vitest";
import { Point, cartesianDistance } from ".";
import { DataPoint, buildTree } from "./buildTree";
import { findNearest } from "./findNearest";
import { memory } from "./memory";

function* radialPoints(
    startRadius: number = 1.0,
    radiusDelta: number = 1.0,
    angleDelta: number = Math.PI / 6
): Generator<DataPoint> {
    let radius = startRadius;
    let angle = 0;
    while (true) {
        yield { point: [radius * Math.cos(angle), radius * Math.sin(angle)] };
        radius += radiusDelta;
        angle += angleDelta;
    }
}

function takeN<T>(iter: Iterable<T>, n: number): T[] {
    const res: T[] = [];
    for (const item of iter) {
        res.push(item);
        if (res.length === n) break;
    }
    return res;
}

function takeWhile<T>(iter: Iterable<T>, predicate: (val: T) => boolean): T[] {
    const res: T[] = [];
    for (const item of iter) {
        if (predicate(item)) res.push(item);
        else break;
    }
    return res;
}

describe("findNearest", () => {
    it("should iterate over nodes in ascending order of distance to search point", () => {
        const [reader, writer] = memory();
        const root = buildTree(takeN(radialPoints(), 10), writer)!;

        const search = findNearest([0, 0], root, reader);
        const distances = [...search].map(({ distance }) => Math.round(distance));

        expect(distances).toEqual([1, 4, 9, 16, 25, 36, 49, 64, 81, 100]);
    });

    it("should handle large trees correctly", () => {
        const [reader, writer] = memory();
        const points = takeN(radialPoints(1.0, 0.1, 0.1), 1000);
        const root = buildTree(points, writer)!;

        const targetRadius = 10;
        const targetPoint: Point = [50, 50];

        const search = findNearest(targetPoint, root, reader);
        const found = takeWhile(search, ({ distance }) => distance < targetRadius ** 2);

        const expected = points.filter(({ point }) => cartesianDistance(targetPoint, point) < targetRadius ** 2);

        expect(found.length).toBeGreaterThan(0);
        expect(found.length).toEqual(expected.length);
    });
});
