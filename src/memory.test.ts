import { describe, expect, it } from "vitest";
import { memory } from "./memory";
import { NodeData } from ".";

describe("memory", () => {
    it("should read what was written", () => {
        const [reader, writer] = memory();
        const data: NodeData = { point: [0, 0], left: undefined, right: undefined };
        const id = writer(data, 0);
        expect(reader(id)).toBe(data);
    });

    it("should throw if node does not exist", () => {
        const [reader, _writer] = memory();
        expect(() => reader(123)).toThrow();
    });
});
