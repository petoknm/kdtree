export interface Heap<T> {
    insert(value: T): void;
    peek(): T | undefined;
    pop(): T | undefined;
}

const getParent = (n: number) => (n - 1) >> 1;
const getChild = (n: number) => 2 * n + 1;

export function makeHeap<T>(cmp: (a: T, b: T) => number): Heap<T> {
    const data: T[] = [];

    const upheap = (index: number) => {
        if (index === 0) return;
        const parent = getParent(index);
        if (cmp(data[index], data[parent]) < 0) {
            const temp = data[parent];
            data[parent] = data[index];
            data[index] = temp;
            upheap(parent);
        }
    };

    const downheap = (index: number) => {
        if (index >= data.length) return;
        const left = getChild(index);
        const right = left + 1;
        const smallerChild = (() => {
            if (data[left] && data[right]) return cmp(data[left], data[right]) < 0 ? left : right;
            if (data[left]) return left;
            if (data[right]) return right;
        })();
        if (!smallerChild) return;
        if (cmp(data[smallerChild], data[index]) < 0) {
            const temp = data[smallerChild];
            data[smallerChild] = data[index];
            data[index] = temp;
            downheap(smallerChild);
        }
    };

    return {
        peek: () => data[0],
        insert: (value) => {
            data.push(value);
            const newIndex = data.length - 1;
            upheap(newIndex);
        },
        pop: () => {
            if (data.length <= 1) return data.pop();

            const popped = data.pop()!;
            const res = data[0];
            data[0] = popped;
            downheap(0);
            return res;
        },
    };
}
