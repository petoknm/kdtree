import { AsyncNodeReader, cartesianDistance, Distance, DistanceFn, NodeID, NodeReader, Point } from ".";
import { makeHeap } from "./heap";

export function* findNearest(
    target: Point,
    root: NodeID,
    reader: NodeReader,
    distanceFn: DistanceFn = cartesianDistance
): Generator<{ id: NodeID; distance: Distance }> {
    const insertedNodes = new Set<NodeID>();
    const minHeap = makeHeap<{ id: NodeID; distance: Distance }>(({ distance: di }, { distance: dj }) => di - dj);

    function inner(n: NodeID, depth: number, maxDistance: number = Infinity) {
        const axis = depth % 2;
        const { point, left, right } = reader(n);

        const distance = distanceFn(target, point);
        if (!insertedNodes.has(n)) {
            minHeap.insert({ id: n, distance });
            insertedNodes.add(n);
        }

        const goLeft = target[axis] <= point[axis];

        const primaryChild = goLeft ? left : right;
        const secondaryChild = goLeft ? right : left;

        const diff = distanceFn([point[axis], 0], [target[axis], 0]);

        if (primaryChild) inner(primaryChild, depth + 1, maxDistance);

        const exploreOtherSide = Math.min(minHeap.peek()!.distance, maxDistance) > diff;
        if (secondaryChild && exploreOtherSide) inner(secondaryChild, depth + 1, maxDistance);
    }

    inner(root, 0);
    yield minHeap.pop()!;

    while (minHeap.peek()) {
        const { distance } = minHeap.peek()!;
        inner(root, 0, distance);
        yield minHeap.pop()!;
    }
}

export async function* findNearestAsync(
    target: Point,
    root: NodeID,
    reader: AsyncNodeReader,
    distanceFn: DistanceFn = cartesianDistance
): AsyncGenerator<{ id: NodeID; distance: Distance }> {
    const insertedNodes = new Set<NodeID>();
    const minHeap = makeHeap<{ id: NodeID; distance: Distance }>(({ distance: di }, { distance: dj }) => di - dj);

    async function inner(n: NodeID, depth: number, maxDistance: number = Infinity) {
        const axis = depth % 2;
        const { point, left, right } = await reader(n);

        const distance = distanceFn(target, point);
        if (!insertedNodes.has(n)) {
            minHeap.insert({ id: n, distance });
            insertedNodes.add(n);
        }

        const goLeft = target[axis] <= point[axis];

        const primaryChild = goLeft ? left : right;
        const secondaryChild = goLeft ? right : left;

        const diff = distanceFn([point[axis], 0], [target[axis], 0]);

        if (primaryChild) await inner(primaryChild, depth + 1, maxDistance);

        const exploreOtherSide = Math.min(minHeap.peek()!.distance, maxDistance) > diff;
        if (secondaryChild && exploreOtherSide) await inner(secondaryChild, depth + 1, maxDistance);
    }

    await inner(root, 0);
    yield minHeap.pop()!;

    while (minHeap.peek()) {
        const { distance } = minHeap.peek()!;
        await inner(root, 0, distance);
        yield minHeap.pop()!;
    }
}
