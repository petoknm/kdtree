import { NodeReader, NodeWriter, NodeData } from ".";

export function memory(): [NodeReader, NodeWriter] {
    const storage: NodeData[] = [];
    return [
        (fakeId) => {
            const id = fakeId - 1;
            if (id in storage) return storage[id];
            else throw new Error("Node not found");
        },
        (data) => {
            storage.push(data);
            return storage.length;
        },
    ];
}
