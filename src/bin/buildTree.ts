import { readFileSync, writeFileSync } from "fs";
import { buildTree, Point } from "..";

const content = readFileSync(0, "utf8");
const json = JSON.parse(content);
const items = json as { name: string, point: Point }[];

const data = new ArrayBuffer(16 * items.length);

let currentIndex = items.length;
const root = buildTree(items, ({ point: [x, y], left, right }) => {
    const id = --currentIndex;
    const offset = 16 * id;
    const view = new DataView(data, offset);
    view.setFloat32(0, x);
    view.setFloat32(4, y);
    view.setUint32(8, left || 0);
    view.setUint32(12, right || 0);
    return id;
});

writeFileSync(1, new DataView(data));
