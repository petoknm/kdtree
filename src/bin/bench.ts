import { findNearest, Point, NodeID } from "..";
import { Bench } from "tinybench";
import { readFileSync } from "fs";

const tree = readFileSync(0).buffer;

const reader = (id: NodeID) => {
    const offset = 16 * id;
    const view = new DataView(tree, offset);
    const x = view.getFloat32(0);
    const y = view.getFloat32(4);
    const left = view.getUint32(8);
    const right = view.getUint32(12);
    return { point: [x, y] as const, left, right };
};

const bench = new Bench({ time: 1000 });

const takeN = <T>(n: number, iter: Iterator<T>): T[] => {
    const res = [];
    while (res.length !== n) res.push(iter.next().value);
    return res;
}

const target: Point = [0.1963558, 48.003451799936876];

const testFn = (n: number) => () => {
    const search = findNearest(target, 0, reader);
    return takeN(n, search);
};

bench.add("lookup 10", testFn(10));
bench.add("lookup 25", testFn(25));
bench.add("lookup 100", testFn(100));

bench.run().then(() => {
    console.table(
        bench.tasks.map(({ name, result }) => ({ "Task Name": name, "Average Time (ms)": result?.mean, "Variance (ms)": result?.variance })),
    );
});
