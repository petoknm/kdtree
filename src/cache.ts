import { AsyncNodeReader, NodeData, NodeID, NodeReader } from ".";

export function cache(reader: NodeReader): NodeReader {
    const cached = new Map<NodeID, NodeData>();
    return (id) => {
        if (cached.has(id)) return cached.get(id)!;
        const value = reader(id);
        cached.set(id, value);
        return value;
    };
}

export function cacheAsync(reader: AsyncNodeReader): AsyncNodeReader {
    const cached = new Map<NodeID, NodeData>();
    return async (id) => {
        if (cached.has(id)) return cached.get(id)!;
        const value = await reader(id);
        cached.set(id, value);
        return value;
    };
}
