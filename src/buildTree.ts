import { NodeData, NodeID, NodeWriter, Point } from ".";

function medianSplit<T>(data: T[], valueFn: (datum: T) => number): [T, T[], T[]] {
    const sorted = Array.from(data);
    sorted.sort((a, b) => valueFn(a) - valueFn(b));
    const middle = sorted.length >> 1;
    return [sorted[middle], sorted.slice(0, middle), sorted.slice(middle + 1)];
}

export interface DataPoint {
    point: Point;
}

export function buildTree<T extends DataPoint>(data: T[], nodeWriter: NodeWriter<T>): NodeID | undefined {
    const inner = (data: T[], depth: number): NodeID | undefined => {
        if (data.length === 0) return undefined;
        const axis = depth % 2;
        const [median, leftData, rightData] = medianSplit(data, ({ point }) => point[axis]);
        const [left, right] = [inner(leftData, depth + 1), inner(rightData, depth + 1)];
        return nodeWriter({ ...median, left, right }, depth);
    };

    return inner(data, 0);
}
