import { describe, it, expect, vi } from "vitest";
import { NodeWriter } from ".";
import { buildTree } from "./buildTree";

const discard: NodeWriter = () => 0;

describe("buildTree", () => {
    it("should return undefined if data is empty", () => {
        expect(buildTree([], discard)).toBeUndefined();
    });

    it("should create a one node tree", () => {
        const writer = vi.fn(discard);
        const root = buildTree([{ point: [0, 0] }], writer);
        expect(writer).toHaveBeenCalledWith({ point: [0, 0], left: undefined, right: undefined }, 0);
    });

    it("should split data in the middle, odd number of points", () => {
        const writer = vi.fn(discard);
        buildTree(
            [
                { point: [-1, 0] },
                { point: [+0, 0] },
                { point: [+1, 0] },
            ],
            writer,
        );
        expect(writer).toHaveBeenLastCalledWith({ point: [0, 0], left: 0, right: 0 }, 0);
    });

    it("should split data in the middle, even number of points, root comes after leaf", () => {
        const writer = vi.fn(discard);
        buildTree(
            [
                { point: [-1, 0] },
                { point: [+0, 0] },
            ],
            writer
        )!;
        expect(writer).toHaveBeenLastCalledWith({ point: [0, 0], left: 0, right: undefined }, 0);
    });

    it("should split data in the middle, even number of points, root comes before leaf", () => {
        const writer = vi.fn(discard);
        buildTree(
            [
                { point: [+1, 0] },
                { point: [+0, 0] },
            ],
            writer
        )!;
        expect(writer).toHaveBeenLastCalledWith({ point: [1, 0], left: 0, right: undefined }, 0);
    });
});
