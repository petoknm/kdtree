export type NodeID = number;
export type Point = readonly [number, number];

export type Distance = number;
export type DistanceFn = (a: Point, b: Point) => Distance;

export interface NodeData {
    point: Point;
    left: NodeID | undefined;
    right: NodeID | undefined;
}

export type NodeReader = (id: NodeID) => NodeData;
export type AsyncNodeReader = (id: NodeID) => Promise<NodeData>;
export type NodeWriter<T = {}> = (data: T & NodeData, depth: number) => NodeID;

export const cartesianDistance = ([x0, y0]: Point, [x1, y1]: Point): Distance => {
    return (x1 - x0) ** 2 + (y1 - y0) ** 2;
};

const { asin, cos, sqrt, PI } = Math;
const toRadians = PI / 180;

export const haversineDistance =
    (radius: number = 6371) =>
        ([lon0, lat0]: Point, [lon1, lat1]: Point): Distance => {
            const rlat0 = lat0 * toRadians;
            const rlat1 = lat1 * toRadians;
            const dlat = rlat1 - rlat0;
            const dlon = (lon1 - lon0) * toRadians;
            const t = 0.5 * (1 - cos(dlat) + cos(rlat0) * cos(rlat1) * (1 - cos(dlon)));
            return 2 * radius * asin(sqrt(t));
        };

export { buildTree } from "./buildTree";
export { findNearest, findNearestAsync } from "./findNearest";
export { cache, cacheAsync } from "./cache";
