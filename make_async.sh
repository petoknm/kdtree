#!/bin/bash

# This script generates the async version of findNearest

inputFile="src/findNearest.ts"

if grep -q findNearestAsync $inputFile; then
    echo "Async function already present, regenerating..." > /dev/stderr
    # Find on which line the async function is defined
    lineno=$(grep -n findNearestAsync $inputFile | cut -d: -f1)
    # Remove lines that contain the async function
    sed -i "$lineno,$$ d" $inputFile
fi

input=$(cat $inputFile)

# transform inputFile
echo "$input" \
    `# remove imports` \
    | tail -n +4 \
    `# update main function signature` \
    | sed 's/export/export async/' \
    | sed 's/findNearest/findNearestAsync/' \
    | sed 's/NodeReader/AsyncNodeReader/' \
    | sed 's/Generator/AsyncGenerator/' \
    `# update inner function signature and call sites` \
    | sed 's/function inner/async function inner/' \
    | sed 's/reader(/await reader(/' \
    | sed '/function/!s/inner/await inner/g' \
    `# append to input file` \
    >> $inputFile
